import React, { Component } from "react";
import _ from "lodash";
import Api from "../../utils/Api";
import Cache from "../../utils/Cache";
import "./Movie.css";
import { Loader } from "../../components";

const MissingImage =
  "https://via.placeholder.com/200X400?text=Missing%20poster";
const MoviesKeysNotToDisplay = [
  "imdbID",
  "type",
  "DVD",
  "BoxOffice",
  "Response"
];

const DataKeysNotToDisplay = ["Ratings", "Poster"];
class Movie extends Component {
  constructor(props) {
    super(props);

    this.state = {
      movie: {},
      loading: true
    };
  }

  componentDidMount = () => {
    let movieID = this.props.match.params.imdb_id;
    if (movieID !== "") {
      this.handleGetMovie(movieID);
    }
  };

  handleGetMovie = imdbID => {
    let cacheMovie = Cache.get(imdbID);
    if (cacheMovie) {
      this.setState({ movie: cacheMovie,loading:false });
    } else {
      this.getMovieById(imdbID);
    }
  };
  getMovieById = async imdbID => {
    try {
      let params = {
        i: imdbID
      };
      let response = await Api.getMovieById(params);
      if (response) {
        response = this.filterNonAvailableValues(response);
      }
      Cache.set(imdbID, response);
      await this.setState({ movie: response, loading: false });
    } catch (e) {
      alert(e);
      this.setState({ loading: false });
    }
  };

  filterNonAvailableValues = data => {
    return _.pickBy(
      data,
      (value, key) => value !== "N/A" && MoviesKeysNotToDisplay.indexOf(key) < 0
    );
  };

  render() {
    const { loading, movie } = this.state;
    if (loading)
      return (
        <div className="movie-container-center">
          <Loader />
        </div>
      );
    let posterImage = movie.Poster && movie.Poster !== "N/A" ? movie.Poster : MissingImage;

    return (
      <div className="movie-container fullscreen">
        {movie && movie.Error ? (
          <div className="movie-container-center">
            <h2>Movie not found</h2>
          </div>
        ) : (
          <div className="movie-wrapper  fullscreen">
            {movie.Poster !== "N/A" && (
              <div className="movie-background-image-wrapper container-flex">
                <div
                  className="movie-background-image"
                  style={{ backgroundImage: `url(${posterImage})` }}
                />
                <div className="movie-image-wrapper">
                  <img src={posterImage} alt="img-poster"></img>
                </div>
              </div>
            )}
            <div className="movie-content container-flex">
              <div className="movie-content-header">
                <h2>Movie Details:</h2>
              </div>

              <h1>{movie.Title}</h1>
              <div className="movie-details">
                {movie &&
                  _.keys(movie).map((k, i) => {
                    if (DataKeysNotToDisplay.indexOf(k) > -1) return null;
                    return (
                      <div key={i} className="movie-details-item">
                        <div className="movie-details-item-key">
                          <h3>{k}</h3>
                        </div>
                        <div className="movie-details-item-value">
                          <p>{movie[k]}</p>
                        </div>
                      </div>
                    );
                  })}
                {movie.Ratings ? (
                  <div className="movie-ratings">
                    <div className="movie-ratings-header">
                      <h2>Ratings</h2>
                    </div>
                    <div className="movie-ratings-details">
                      {movie.Ratings.map((r, i) => (
                        <div key={i} className="movie-rating-item">
                          <div className="movie-rating-value">
                            <p>{r.Value}</p>
                          </div>
                          <h3>{r.Source}</h3>
                        </div>
                      ))}
                    </div>
                  </div>
                ) : null}
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default Movie;
