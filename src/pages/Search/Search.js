import React, { Component } from "react";
import { SearchInput, Loader, MoviesList } from "../../components";
import Api from "../../utils/Api";
import "./Search.css";
import Cache from "../../utils/Cache";

const IMAGE_LOGO = require("../../assets/rapid_movie_logo.png");

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      isSearching: false,
      initValue: "",
      movies: []
    };
  }

  componentDidMount() {
    let queryMovie = this.getQueryMovie();
    if (queryMovie) {
      this.setState({ initValue: queryMovie });
      this.handleSearchMovie(queryMovie);
    }
  }

  getQueryMovie = () => {
    const params = new URLSearchParams(this.props.location.search);
    return params.get("q");
  };


  handleSearchMovie = movieTitle => {
    let cacheMovies = Cache.get(movieTitle);
    if (cacheMovies) {
      this.setState({ movies: cacheMovies,isSearching:true });
    } else {
      this.onSearchMovie(movieTitle);
    }
  };
  
  onSearchMovie = async movieTitle => {
    this.setState({ loading: true });

    try {
      let params = {
        type: "movie",
        s: movieTitle
      };
      let response = await Api.searchMovie(params);
      this.setState({
        movies: response.Search,
        loading: false,
        isSearching: true
      });
      if(movieTitle ){
        Cache.set(movieTitle,response.Search)

      }
    } catch (e) {
      this.setState({ loading: false,movies:[], isSearching: true });
    }
  };
  render() {
    const { loading, isSearching, initValue, movies } = this.state;
    return (
      <div className="search-container">
        <div className="search-container-wrapper">
          <img src={IMAGE_LOGO} alt="rapid_movie_logo" />
        </div>
        <div className="search-container-movies">
          <SearchInput
            initValue={initValue}
            onSearchMovie={this.handleSearchMovie}
          />
          {loading && <Loader />}

          {!loading && isSearching && <MoviesList movies={movies} />}
        </div>
      </div>
    );
  }
}

export default Search;
