import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Search, Movie } from "./pages";

class App extends Component {
  render() {
    return (
      <div className="fullscreen">
      <Router>
        <Route exact path="/" component={Search} />
        <Route exact path="/:imdb_id" component={Movie} />
      </Router>
      </div>
    );
  }
}

export default App;
