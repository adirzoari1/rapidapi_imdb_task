
class Rest {

  withQuery(url, params) {
    let query = Object.keys(params)
      .filter(k => params[k] !== undefined)
      .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
      .join('&')
    url += (url.indexOf('?') === -1 ? '?' : '&') + query
    return url
  }



  async send(method,params) {

    let url = `${process.env.REACT_APP_API_URL}/?apikey=${process.env.REACT_APP_API_KEY}`
    if (params) {
      url = this.withQuery(url, params)
    }
    try {
      let response = await fetch(url, {
        method: method,
      })

      
      let responseJson = await response.json();
      if (response.status !== 200) {
        return Promise.reject(responseJson)

      }
      return responseJson
    } catch (error) {
      console.error(error)
    }
  }
}

export default new Rest()
