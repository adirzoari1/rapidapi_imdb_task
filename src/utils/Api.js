import Rest from './Rest'

class Api {
  searchMovie(params) {
    return Rest.send('GET',params)
  }

  getMovieById(params){
    return Rest.send('GET',params)

  }

}

export default new Api()
