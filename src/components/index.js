import Loader from "./Loader";
import SearchInput from "./SearchInput";
import MoviesList from "./MoviesList";
import MovieItem from "./MovieItem";
export { Loader, SearchInput, MoviesList, MovieItem };
