import React from "react";
import "./MoviesList.css";
import MovieItem from "../MovieItem";

const MoviesList = ({ movies }) => (
  <div className="movies-container">
 
    {movies && movies.length > 0 ? (
      <div className="movies-list-wrapper">
        <div className="movies-list-container">
      {movies && movies.map(movie => <MovieItem key={movie.imdbID} {...movie} />)}
    </div>
    </div>
      ) : (
      <h2>No movies found..please try again</h2>
    )}
  </div>
);

export default MoviesList;
