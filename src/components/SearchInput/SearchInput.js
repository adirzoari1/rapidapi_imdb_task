import React, { Component } from "react";
import _ from "lodash";
import "./SearchInput.css";

const DEBOUNCE_TIME = 600
class SearchInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.initValue || ''
    };
    this.debounceOnSearch = _.debounce(this.props.onSearchMovie, DEBOUNCE_TIME);
  }

  componentDidUpdate = prevProps => {
    if (prevProps.initValue !== this.props.initValue) {
      this.setState({ value: this.props.initValue });
    }
  };
  onChangeInput = e => {
    this.setState({ value: e.target.value });
    this.debounceOnSearch(e.target.value);
  };

  render() {
    return (
      <div className="search-input-container">
          <input
            type="text"
            placeholder="Search movie ..."
            onChange={this.onChangeInput}
            value={this.state.value}
          />
      </div>
    );
  }
}

export default SearchInput;
