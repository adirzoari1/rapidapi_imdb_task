import React from "react";
import "./MovieItem.css";
import { Link } from "react-router-dom";
const MissingImage =
  "https://via.placeholder.com/200X400?text=Missing%20poster";

const MovieItem = ({ Poster, Title, Year, imdbID }) => {
  let posterImage = Poster === "N/A" ? MissingImage : Poster;
  return (
    <Link to={`/${imdbID}`}>

    <div className="movie-item-container" >
          <img src={posterImage} alt={Title} />
          <div className="movie-item-content">
            <h3>{Title}</h3>
            <p>{Year}</p>
          </div>

    </div>
    </Link>

  );
};

export default MovieItem;
